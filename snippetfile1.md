# SDK Externe et API Tierces

Objectif : Lister ce à quoi les SDK externes et API tierces doivent se conformer afin d'être intégrés dans l'application PagesJaunes. Le but est de ne pas limiter PagesJaunes dans son évolution a cause d'un SDK mal conçu.


## Besoin

- [x] On doit pouvoir influer sur les évolutions possibles du SDK afin qu'il puisse évoluer avec nous si besoin.
- [x] **On doit avoir un contact technique direct** (et non pas un quelqu'un du support commercial). Cette personne doit avoir la possibilité d'influer sur l'évolution du SDK et nous renseigner techniquement dans un délai acceptable.
- [x] On doit avoir la possibilité d'envoyer des crashs lié au SDK et de suivre l'évolution de la correction de ces derniers.
- [x] **La consommation réseau doit être raisonnable** (faire le moins de requêtes possibles, grouper les requêtes, renvoyer les requêtes échouées au prochain lancement de l'app) et si possible les requêtes moins importantes devraient être étalées dans le temps afin de ne pas impacter négativement l'experience utilisateur.
- [x] Avoir une interface de test / debug / une console de test dans leur back-office et ou une API afin de simuler/forcer le fonctionnement du SDK est un plus non négligeable car il nous permettrait d'automatiser les tests de ce dernier dans notre application.
- [x] PagesJaunes veut avoir la possibilité de voir le code qui rentre dans son app et donc préfère ne pas utiliser de SDK avec des sources fermées. Car ce dernier peut faire des actions qui vont a l'encontre de nos CGU et donc pour nous le seul moyen de garantir de qui ce passe est d'avoir du code open source (ou avoir accès au repository privé) et pouvoir auditer toutes les requêtes effectuer par le SDK(a travers l'utilisation de [**NSURLProtocol**](https://developer.apple.com/reference/foundation/nsurlprotocol) par exemple sur iOS). On a eu le problème dans le passé avec des SDK qui récupérait des informations utilisateurs sans notre consentement donc dorénavant, nous sommes ferme sur ces questions.
- [x] Respecter la vie privée des utilisateurs et correctement sécuriser les données exploitées ( [User Privacy and Data Use](https://developer.apple.com/app-store/user-privacy-and-data-use/) / [Security Overview](https://developer.apple.com/library/ios/documentation/Security/Conceptual/Security_Overview/Introduction/Introduction.html#//apple_ref/doc/uid/TP30000976) /  [Secure Coding Guide](https://developer.apple.com/library/ios/documentation/Security/Conceptual/SecureCodingGuide/Introduction.html#//apple_ref/doc/uid/TP40002415))
- [x] Le SDK doit être rapidement et complètement désactivable à distance par le fournisseur. C'est a dire désactiver tous les appels du SDK pour un temps donné (de 1h a 8000 ans). A l'initialisation de ce dernier, la premiere chose à vérifier par le SDK est s'il est autoriser à effectuer des requêtes.
- [x] L'entreprise doit maitriser la technologie qu'elle nous met a disposition. C'est à dire qu'elle ne peux pas utiliser un SDK tiers qu'elle ne maitrise pas. Dans le cas échéant, nous serions confrontés à ces problématiques :

  - on aura trop d'adhérence en cas d'évolution
  - on ne maîtrisera pas ce qui sont fait de nos données utilisateur
  - on aura encore moins de compréhension de ce qui est fait dans notre application
  - les problèmes de sécurité / et juridique associé seront encore plus élevé

## Réseaux / API

### Sécurité

Toutes les requêtes doivent être en HTTPS:

- [x] le certificat doit être signé au minimum en SHA256 avec une clé soit en RSA en 2048-bit au minimum ou Elliptic-Curve (ECC) en 256-bit au minimum
- [x] le serveur doit supporter TLS 1.2 (au minimum) ou TLS 1.3
- [x] pour la connexion, la liste des ciphers supportés sont ceux compatibles avec le [forward secrecy](https://en.wikipedia.org/wiki/Forward_secrecy)
- [x] sécurité réseaux iOS (WWDC 2017 -  [Your Apps and Evolving Network Security Standards](https://developer.apple.com/videos/play/wwdc2017/701/) /  [https://developer.apple.com/videos/play/wwdc2016/706/](https://developer.apple.com/videos/play/wwdc2016/706/))
- [x] support de l'HSTS -  [https://en.wikipedia.org/wiki/HTTP\_Strict\_Transport\_Security](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security)



### Performance

- [ ] le temps de réponse moyen doit être **inférieur à 750 ms**
- [ ] le timeout côté serveur doit être **inférieur à 1000 ms**
- [x] l'utilisation de **HTTP/2 est un plus appréciable**
- [ ] le [**support de l'IPv6**](https://developer.apple.com/support/ipv6/) est primordiale

### Documentation

- WWDC 2019 -  [Advances in Networking, Part 1](https://developer.apple.com/videos/play/wwdc2019/712/)
- WWDC 2019 -  [Advances in Networking, Part 2](https://developer.apple.com/videos/play/wwdc2019/713/)

# Le SDK ne doit pas:

- [x] faire de **swizzling**
- [x] **bloquer le main thread** 

# Le SDK doit:

## **Compatibilité:**

- [x] supporter les OS >= iOS 13
- [x] être compatibles avec les nouvelles versions d'iOS et d'Xcode  **dès leur sortie**  (versions betas et Releases).  **Le SDK ne doit pas nous empêcher d'évoluer / avancer**
- [x] être  **compatible avec Swift 5**
- [x] doit faire toutes ses requêtes en  **HTTPS**
- [x] supporter du [**Bitcode**](https://lowlevelbits.org/bitcode-demystified/) / [**App Slicing**](https://developer.apple.com/library/content/documentation/IDEs/Conceptual/AppDistributionGuide/AppThinning/AppThinning.html) ([App Thinning in Xcode](https://developer.apple.com/videos/play/wwdc2015-404/))
- [x] supporter le  [semantic versioning](http://semver.org)
- [ ] si nécessaire doit être compatible avec les extensions (Widget / Siri / Clavier etc.) ainsi que avec les autres plateformes (watchOS / tvOS)

## **Fonctionnalités appréciables:**


- [x] avec une API bien structurée  [Better Translation of Objective-C APIs Into Swift](https://github.com/apple/swift-evolution/blob/master/proposals/0005-objective-c-name-translation.md)
- [x] la documentation du SDK doit être visible depuis Xcode

## **Accessibilité:**

- [x] si présence d'UI : l'API doit  **être accessible**  -&gt; on doit pouvoir utiliser entièrement ses interfaces avec voice over ( [iOS Accessibility](https://developer.apple.com/videos/play/wwdc2015-201/) /  [Accessibility on iOS](https://developer.apple.com/videos/play/wwdc2014-210/) /  [Accessibility Programming Guide for iOS](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/iPhoneAccessibility/Introduction/Introduction.html#//apple_ref/doc/uid/TP40008785))


## **Dark mode:**
- [x] doit supporter le light et le dark mode

## **SDK:**

- [x] être fournis via un gestionnaire de dépendances tel que [Swift Package Manager](https://swift.org/package-manager/) (ou si impossible techniquement : [Cocoapods](https://cocoapods.org/))
- [x] si il y a des ressources associées au SDK, elles doivent faire partie du package récupéré par le gestionnaire de dépendance
- [x] démarrer uniquement si on lui demande (**ne doit pas démarrer tout seul** / **ne doit pas impacter l'app sans notre demandé** )
- [x] éviter d'avoir des dépendances à des librairies tierces (AFNetworking, Reachability, etc...). Si c'est le cas,  **elles doivent être préfixées**

- [x] avoir une API simple et descriptive comme celle d'Apple ( [Coding Guidelines for Cocoa](https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html#//apple_ref/doc/uid/10000146i) /  [Building Responsive and Efficient Apps with GCD](https://developer.apple.com/videos/play/wwdc2015-718/) /  [Better Translation of Objective-C APIs Into Swift](https://developer.apple.com/library/ios/documentation/Security/Conceptual/Security_Overview/Introduction/Introduction.html#//apple_ref/doc/uid/TP30000976)) et réaliser avec des techniques actuelles ( [Adopting Modern Objective-C](https://developer.apple.com/library/ios/releasenotes/ObjectiveC/ModernizationObjC/AdoptingModernObjective-C/AdoptingModernObjective-C.html#//apple_ref/doc/uid/TP40014150))


- [x] doit supporter le  **lowPowerModeEnabled** ( [docs](https://developer.apple.com/documentation/foundation/nsprocessinfo/1617047-lowpowermodeenabled)) et réduire décaler ces requêtes et réduire sont travail au minimum voir ce désactiver dans ce cas
- [x] doit **préférer les technologie native a iOS** plutôt que de re-inventer la roue (par exemple utiliser ARKit si le SDK fait de la réalité augmenter ou Vision si il analyse des visages ou encore CoreML/CoreImage si il fait de la reconnaissance d'image)

## **Debug:**

- [ ] pouvoir activer le log avec différents niveaux de verbosité ou au contraire de  **désactiver le log du SDK à 100%**
- [x] nommer les threads utilisés pour qu'on puisse debugger en configurant la propriété  **name ** de  **NSOperationQueue**
- [ ] **être livré avec une app ou des apps de test qui utilise **** toutes **** ses APIs et qui compile**
- [ ] si le SDK en besoin le mieux est qu'il ai sont propre fichier de configuration de type plist dissocier de l'info.plist afin de lui appliquer une configuration custom

## **Performance:**

- [x] **impacter le moins possible l'application hôte**  d'un point du vue CPU, GPU, GPS, NETWORK, I/O  ([Writing Energy Efficient Code, Part 1](https://developer.apple.com/videos/play/wwdc2014-710/) / [Writing Energy Efficient Code, Part 2](https://developer.apple.com/videos/play/wwdc2014-712/) / [Profiling in Depth](https://developer.apple.com/videos/play/wwdc2015-412/) / [Building Responsive and Efficient Apps with GCD](https://developer.apple.com/videos/play/wwdc2015-718/) / [Debugging Energy Issues](https://developer.apple.com/videos/play/wwdc2015-708/))
- [x] éviter l'utilisation de timers ([Writing Energy Efficient Code, Part 1](https://developer.apple.com/videos/play/wwdc2014-710/) / [Writing Energy Efficient Code, Part 2](https://developer.apple.com/videos/play/wwdc2014-712/))
- [x] être sur d'utiliser les dernières bonne pratique en terme de optimisation de consommation d'énergie(WWDC 2017 - [Writing Energy Efficient Apps](https://developer.apple.com/videos/play/wwdc2017/238/))
- [x] doit  **préférer les technologie native a iOS ** si le SDK a besoin de faire des calculs avancé(compute) il doit utiliser Metal 2( [Using Metal 2 for Compute](https://developer.apple.com/videos/play/wwdc2017/608/)) ou Accelerate( [Accelerate and Sparse Solvers](https://developer.apple.com/videos/play/wwdc2017/711/))

